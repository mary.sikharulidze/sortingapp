import org.example.Main;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * MoreThan10Test is Parameterized test with private field, constructor and Test case
 */
@RunWith(Parameterized.class)
public class MoreThan10Test{
    private int[] inputArray;

    public MoreThan10Test(int[] inputArray) {
        this.inputArray = inputArray;
    }

    /**
     * Parameterized test case, which throw IllegalArgumentException if number of elements in array are more than 10
     */
    @Test(expected = IllegalArgumentException.class)
    @Parameterized.Parameters
    public void testMoreThan10Case() {
        Main.sort(inputArray);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> ParamMoreThan10Data() {
        return Arrays.asList(new Object[][]{
                {new int[]{1, 3, 5, 5, 6, 8, 11, 12, 12, 22, 4, 8}},
                {new int[]{1, 3, 5, 5, 6, 8, 11, 12, 12, 22, 4, 8, 7, 18}}
        });
    }
}