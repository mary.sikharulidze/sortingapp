import org.example.Main;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Sorting App Test is Parameterized test with private fields, constructor and Test cases.
 */
@RunWith(Parameterized.class)
public class SortingAppTest {
    private int[] inputArray;
    private int[]  expectedArray;

    public SortingAppTest(int[] inputArray, int[] expectedArray) {
        this.inputArray = inputArray;
        this.expectedArray = expectedArray;
    }

    /**
     * if array is null throws NullPointerException
     */
    @Test(expected = NullPointerException.class)
    public void testNullCase() {
        Main.sort(null);
    }

    /**
     * checks if array is empty
     */
    @Test
    public void testEmptyCase() {
        int[] arr = new int[]{};
        Main.sort(arr);
        assertEquals(0, arr.length);
    }

    /**
     * checks if array consists of single element
     */
    @Test
    public void testSingleElementCase() {
        int[] arr = new int[]{1};
        Main.sort(arr);
        assertEquals(1, arr.length);
        assertEquals(1, arr[0]);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> ParametersData() {
        return Arrays.asList(new Object[][]{
                {new int[]{25, 1, 6, 2, 7, 5, 8, 12, 31, 3}, new int[]{1, 2, 3, 5, 6, 7, 8, 12, 25, 31}},
                {new int[]{22, 12, 11, 12, 8, 6, 5, 5, 3, 1}, new int[]{1, 3, 5, 5, 6, 8, 11, 12, 12, 22}},
        });
    }

    /**
     * Parameterized test case, which checks if method works correctly, given input and output arrays.
     */
    @Test
    @Parameterized.Parameters
    public void test10ElementArrayCase() {
        Main.sort(inputArray);
        assertArrayEquals(expectedArray, inputArray);
    }



}




