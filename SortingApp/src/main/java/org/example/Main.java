
package org.example;


/**
 * Main classs with 3 methods for sorting positive numbers
 */
public class Main {
    /**
     * main() method takes command-line arguments (array of strings), sorts them in the ascending order, and then prints
     * them into standard output.
     */
    public static void main(String[] args) {
        if (args == null || args.length == 0) throw new IllegalArgumentException();
        if (args.length > 10) throw new IllegalArgumentException();

        int[] arr = ParseInput(args);
        sort(arr);
        for (int ar : arr) {
            System.out.print(ar + " ");
        }
    }

    /**
     * sort() method sorts the given array of ints and returns nothing;
     */
    public static void sort(int[] array) {
        if (array.length > 10) throw new IllegalArgumentException();
        for (int i = 1; i < array.length; i++) {
            int j = i;
            while (j > 0 && array[j] < array[j - 1]) {
                int temp = array[j];
                array[j] = array[j - 1];
                array[j - 1] = temp;
                j--;
            }
        }
    }

    /**
     * ParseInput() method parses input and catches number format exception if it cant parse array of strings to array of integers.
     * returns array of integers
     */
    public static int[] ParseInput(String[] args) {
        int[] arr = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                arr[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                System.out.println(e + " invalid input");
            }
        }
        return arr;
    }
}